$(document).ready(function(){
	
	function changeVideo(weatherImage) {
		
		// Change video source if it needs to be

		if (weatherImage !== $('video source:first-of-type').attr("src").split(".")[0]) {

			$('#curtain').addClass('revealed').removeClass('hidden');

			setTimeout(function(){

				$('video source:first-of-type').attr("src", weatherImage + ".webm")
				.next("source").attr("src", weatherImage + ".mp4");
				$('video').attr("poster", weatherImage + ".jpg");
				$('video')[0].load(function(){
					$('#curtain').removeClass('revealed').addClass('hidden');
				});

				// If something goes wrong, just reveal anyway

				setTimeout(function(){
					$('#curtain').removeClass('revealed').addClass('hidden');
				}, 400);

			}, 400);

		}

		
	}
	
	function getHeader(search, desc, time, date) {
		
		// Builds string for display in the header
		
		var first = "<span class=\"mute\">There's ";
		
		// If there's an S at the end it's probably a plural
		
		if (desc.charAt(desc.length-1) === "s") {
			
			first = "<span class=\"mute\">There are ";
			
		} else if (desc === "thunderstorm" || desc === "clear sky") {
			
			first = "<span class=\"mute\">There's a ";
			
		}
		
		if (search) {
			
			time = time + " GMT";
			
		}
		
		// Assemble and return
		
		return first + "</span>" + desc + "<span class=\"mute\"> as of " + time + " on " + date + "</span>";
		
	}
	
	function getIcon(str) {
		
		// Takes a string from openweathermap's JSON weather array called icon and interprets it for our own icon
		// Improve by reading result.weather[0].id instead, that should make for more accurate representations 
		
		if (str === "01") {
			
			return "sun";
			
		} else if (str === "02") {
			
			return "sun-cloud";
			
		} else if (str === "03") {
			
			return "cloud";
			
		} else if (str === "04") {
			
			return "cloud";
			
		} else if (str === "09") {
			
			return "rain";
			
		} else if (str === "10") {
			
			return "heavy-rain";
			
		} else if (str === "11") {
			
			return "thunderstorm";
			
		} else if (str === "13") {
			
			return "snow";
			
		} else if (str === "50") {
			
			return "cloud";
			
		} else {
			
			// We could also guess by reading result.clouds.all (which is a percentage of cloudiness)
			// result.rain.3h and result.snow.3h if they exist, falling back on a default otherwise
			
			return "cloud";
			
		}
		
	}
	
	function convertWind(deg) {
			
		// Takes a degree (from 0 to 360), determines its direction and returns a DOM-ready string
		
		if (deg) {
			
			deg = Math.round(deg*10)/10;

			var windDirection;

			if ((deg >= 337.6 && deg <= 360) || (deg >= 0 && deg <= 22.5)) {

				windDirection = "Northerly";

			} else if (deg >= 22.6 && deg <= 67.5) {

				windDirection = "North Easterly";

			} else if (deg >= 67.6 && deg <= 112.5) {

				windDirection = "Easterly";

			} else if (deg >= 112.6 && deg <= 157.5) {

				windDirection = "South Easterly";

			} else if (deg >= 157.6 && deg <= 202.5) {

				windDirection = "Southerly";

			} else if (deg >= 202.6 && deg <= 247.5) {

				windDirection = "South Westerly";

			} else if (deg >= 247.6 && deg <= 292.5) {

				windDirection = "Westerly";

			} else if (deg >= 292.6 && deg <= 337.5) {

				windDirection = "North Westerly";

			}

			windDirection += ' winds <span class="mute">(' + deg + '°)</span>';

			return windDirection;
			
		} else {
			
			return "Winds of an indeterminate direction";
			
		}
		
		
	}
	
	function stringWind (windDirection, windSpeed) {
		
		// Builds and returns a DOM-ready string describing the winds
		
		var str  = "And there are " + windDirection.toLowerCase();
			str += " going at " + Math.round((windSpeed)/1609.34*3600);
			str += '<span class="mute"> mi/h </span>';
		
		return str;

	}
	
	function goBack(context) {
		
		// Go back to let user reselect their country (from cities) by applying classes with CSS transitions
		
		if (context === "cities") {
			
			$('#city').removeClass('revealed').addClass('hidden');
			$('#country').addClass('revealed').removeClass('hidden');
			$('#country select option:first-of-type').prop("selected", true);
			$('#city select option:first-of-type').prop("selected", true);
			
		} else if (context === "weather") {
			
		// Go back to let user reselect country after having selected a city (from weather)
		
			$('#weather').removeClass('revealed').addClass('hidden');
			$('#country').addClass('revealed').removeClass('hidden');
			$('form').addClass('revealed').removeClass('hidden');
			$('#country select option:first-of-type').prop("selected", true);
			$('#city select option:first-of-type').prop("selected", true);
		
		}
		
	}
	
	function showWeather() {
		
		// Reveal weather data by applying classes with CSS transitions
		
		$('#city').removeClass('revealed').addClass('hidden').addClass('left-hidden');
		$('#country').removeClass('revealed').addClass('hidden');
		$('#weather').addClass('revealed').removeClass('hidden');
		
		setTimeout(function(){
			
			$('#city').addClass('up-hidden');
			
			setTimeout(function(){
				
				$('#city').removeClass('up-hidden left-hidden');
				
			},400);
			
		},400);
		
	}
		
	function showCities(val, str) {
		
		// Get and display list of cities, reveal when successfully completed
		
		var url = val + '-cities.html';
		
		$.get(url,function(options){
			$('#city select').html(options);
			$('#city select option:first-of-type').html("Which city in " + str + "?");

		}).done(function(){
			
			$('#city').addClass('revealed').removeClass('hidden');
			
		}).fail(function(){
			
			// Remove option with val or str that was passed in?
			// Display error and ask user to try again?
			
		});
		
	}
	
	function geoWeather(position) {
		
			var lat = position.coords.latitude;
			var lon = position.coords.longitude;
		
			$('form .find').click(function(){
				
				getWeather([lat,lon],false,false);
				
			});
		
			// Geolocating from #weather must display an animation to maintain a consistent UX
		
			$('#weather .find').click(function(){
				
				$('#weather').removeClass('revealed').addClass('hidden');
				
				// Reveals again after animation
				
				setTimeout(function(){
					
					getWeather([lat,lon],false,false);
					$('#weather').removeClass('hidden').addClass('revealed');

				},400); 
				
			});

		
	}
	
	function getWeather(geo,search,city) {
				
		// Get weather accepts two arguments
		// geo, which must be an array containing latitude and longitude for geolocation,
		// and city which is a string that we'll otherwise use to query the API
		
		var url;
		
		if (!geo) {
			
			url = "http://api.openweathermap.org/data/2.5/weather?q=";
			url += city.toLowerCase(); // + ",uk";
			url += "&units=metric";
			url += "&appid=74f8e01f4c77eedd8ee043a870bf1c07";
			
		} else {

			var lat = geo[0];
			var lon = geo[1];
			
			url = "http://api.openweathermap.org/data/2.5/weather?lat=";
			url += lat;
			url += '&lon=';
			url += lon;
			url += "&units=metric";
			url += "&appid=74f8e01f4c77eedd8ee043a870bf1c07";
			
		}
		
		// Get the weather
		
		$.get(url,function(result){
			
			// Reset DOM
			$('#weather .box').removeClass('error');
			$('#temp').html("");
			$('#desc').html("");
			$('#wind').html("");
			
			// Build date string
		
			var currentDate = new Date();
			var dateString 	= currentDate.getDate() + "/";
				dateString += ("0" + (currentDate.getMonth()+1)).slice(-2) + "/";
				dateString += currentDate.getFullYear();

			// Build time string

			var mins = ("0" + currentDate.getMinutes()).slice(-2);
			var time = currentDate.getHours() + ":" + mins;

			// Format data
			
			city = result.name;
			var message = getHeader(search, result.weather[0].description, time, dateString);
			var temp = Math.round(result.main.temp) + '<span class="mute">° C in </span>' + city ;
			if (search && result.sys.country) {
				temp += ", " + result.sys.country;
			}
			var windDirection = convertWind(result.wind.deg);
			var wind = stringWind(windDirection, result.wind.speed);
			var weatherImage = getIcon(result.weather[0].icon.slice(0,2));
						
			// Roll out changes
			
			changeVideo(weatherImage);
			$('#weather .box').css({backgroundImage: 'url(light-icons/' + weatherImage + '.png)'});
			$('#temp').html('<span class=\"temp\">It\'s ' + temp + "</span><br>");
			$('#desc').html(message);
			$('#wind').html(wind);
			
			console.log(result);
			
		})

		// Then reveal afterward when ready
		
		.done(showWeather)
		
		// In the case of an error, let the user know
		
		.fail(function(){
			
			showWeather();
			$('#wind').html("");
			
			if (city) {
				$('#weather .box').addClass('error');
				$('#temp').html("Sorry :(");
				$('#desc').html("We couldn't get the weather for " + city);
			} else if (search) {
				$('#weather .box').addClass('error');
				$('#temp').html("We didn't get that :(");
				$('#desc').html("Please try again");
			}
			
			
		});

	}
	
	function init() {
		
		// Binds events to tie everything together
		
		$('#country select').change(function(){
			
			var val = $('#country select option:selected').attr('value');
			var str = $('#country select option:selected').text();
			
			if (val) {
				
				showCities(val, str);
				$('#country').removeClass('revealed').addClass('hidden');
				
			}

		});
		
		$('#city select').change(function(){
			
			var city = $('#city select option:selected').text();
			
			if ($('#city select option:selected').prev().is("option")) {
				
				getWeather(false,false,city);
				
			}
			
		});

		$('#city .back').click(function(){
			
			goBack("cities");
			
		});
		
		$('#weather .back').click(function(){
			
			goBack("weather");
			
		});
		
		$('.search.button').click(function(){
			
			var city = $('#search').val();
			
			$('#search').val("");
			
			getWeather(false,true,city);
			
			return false;
			
		});
		
		if (navigator.geolocation) {
			
			navigator.geolocation.getCurrentPosition(geoWeather);
		
		} else {
			
			$('.find').hide();
			
		}

		
	}
	
	// Go
	
	init();

});